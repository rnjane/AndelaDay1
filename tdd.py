import unittest

from temp_converter import temp_converter
class TempConverterTets(unittest.TestCase):
    def test_celsius_is_converted_to_fahrenheit(self):
        """
        F = C * 9/5 + 32
        """
        actual = temp_converter(10)
        expected = 50
        self.assertEquals(actual, expected, 'Conversion is not working OK')
        self.assertEqual(temp_converter(20), 68)
        

